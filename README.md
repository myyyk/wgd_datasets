# README #

This repository contains two datasets E and Omega for testing whole genome duplication search software. Each of the datasets consists of three sets of rooted gene trees (inferred with ML method) and a species tree. 
Additionally we present csv files with results obtained by using *gdilp* software available at https://gitlab.uw.edu.pl/j.paszek/genomicduplicationilp. 

 

### Dataset E ###

Dataset was built using genomes of 6 plant species: *A.thaliana*, *A.lyrata*, *C.rubella*, *B.rapa*, *S.parvula*,*T.hassleriana*.

List of files:

**all_ml_trees_rooted** -- all inferred gene trees.

**trees_ml_max3repeatspec_rooted** -- gene trees containing up to three genes per species.

**trees_ml_no0branches_rooted** -- set with gene trees without any branches of zero lenght.

**speciestree** -- file containing the species tree.

**names_translation** -- translation from short names to full species names.

**gdlip_results** -- directory with *gdilp* results in csv format


### Dataset Omega ###

Dataset was built using genomes of 18 plant species: *A.thaliana*,*A.lyrata*,*B.rapa*, *T.cacao*, *B.vulgaris*, *C.rubella*, *G.max*, *L.sativa*, *M.acuminata*, *M.domestica*, *M.truncatula*, *M.guttatus*, *O.sativa*, *P.persica*, *S.parvula*, *Z.mays*, *A.officinalis*, *S.tuberosum*.

List of files:

**all_ml_trees_rooted** -- all inferred gene trees.

**trees_ml_max9repeatspec_rooted** -- gene trees containing up to nine genes per species.

**trees_ml_max5_0branches_rooted** -- gene trees containing up to 5 branches of zero lenght.

**speciestree** -- file containing the species tree.

**names_translation** -- translation from short names to full species names.


**gdlip_results** -- directory with *gdilp* results in csv format


